-- See https://gitlab.haskell.org/ghc/ghc/issues/17478

{- General idea for vars (equivocating between lambda and let and case binders):
If something is top-level or lambda bound, increase shadowing in the binder's Type and IdInfo, and point occurences to the binder.
If something is defined out of module, it does not refer to anything in this module (except maybe top-level things, via boot files?), so leave it('s IdInfo) alone, ensuring we don't capture any of its FVs.
For binders (top-level and lambda), change its IdInfo & Type to increase shadowing.

We never change the unique of a top-level thing.

So, in analysis:
For an out-of-module occurence, we don't modify its IdInfo, so just record its FVs, which we must not shadow.
For a top-level or local occurence, ignore its info and use the remembered one, as that is the code that will appear after renaming.
For a top-level or local binder, analyse its info, and remember the analysis for use in the body.
NB: the var may occur inside say its unfolding, but we can pretend such occurences are unimportant for shadowing, as (after renaming)
    they will point back to the binder. Thus the info should be the same as the binder, except with perhaps more shadowing opportunities,
    which we are free to ignore (e.g. if the occurence is under a binder x, then a binder y inside the occurences' unfolding may shadow x).
    HOWEVER, there are worries about recursive groups of binders, see below.

And in renaming:
For an out-of-module occurence, leave it alone.
For a local occurence, point it to the binder.
For a top-level or lambda binder, substitute inside its IdInfo/Type.
NB: we need to knot tie here, as e.g. the unfolding of a binder may be recursive and refer to the binder.

Because we may change the (uniques in the) IdInfo on a top-level binding, and this could be referred to elsewhere in the program,
we need to make the renaming consistent across the whole program. Otherwise, we may have foo = \x . bar[Unf=...x,y...], where
we rename the two 'x's differently, breaking the fact that the binder binds that occurence, and rename the \x and y to be the same,
accidentally capturing y.
We analyse the program as a whole. Note that we could potentially have two independent vars sharing the same unique,
perhaps in two different top level definitions. Then we consider what to rename this unique to, and we may have less choice
than if we did them seperately, perhaps introducing less shadowing that we may have been able to.
TODO: is it worth adding in a first pass that ensures all uniques are actually unique, to avoid this?
      Alternatively, how much of this does GHC actually ensure?

Worries about recursive binding groups:
We could have letrec {foo = ..., bar = ...}, where the unfolding of foo mentions bar and vv.
We certainly do not want to recurse into the idinfo of the occurences, both because that would
give an infinite loop, and be incorrect (we will point the occurence to the binder when we rename).
For correctness, we actually want to recurse into the idinfo of the binder of the occurence, but
that would give the same looping problems!
So: let's make two passes: first, find the free vars of the binders (hard); second, do the shadowing analysis,
using the result of the first pass for occurrences of the binders (easy).
For the first pass, let's consider
  foo : Unf = \x -> append [x] bar
  bar : Unf = foo 1
Then we want to make one pass to discover (assuming append and [] have no IdInfo: NOT TRUE)
  fooFVs : append [] bar  + barFVs\x
  barFVs : foo + fooFVs
and then iterate from fooFVs = [], barFVs = [], until hit fixed point.

HOWEVER: we need to care about the IdInfo of out-of-module Ids, which may be marked as INLINEABLE.
         (If they are defined in this module, we will have already analysed them, and can just look up their FVs.)
QUESTION: what does GHC say the FVs of foo, bar, and out-of-module Ids are, with idFreeVars?
 (it claims not to look in IdInfo when it is a dup of the RHS, so get exponential behaviour,
  but I don't know when this happens - perhaps it does look if it is out-of-module?)
ANSWER: GHC will tell us the fvs of the unfolding IF IT IS "STABLE", which is fine if out-of-module I think.
        But for foo and bar, it will say no free vars (unless they are marked as inline), since the unfolding
        is marked as uf_src = InlineRhs, so is skipped.
Thus it looks like I will need to do the traversals manually.
-}

-- NB: we follow CoreSubst, and set idinfo of occurences to point to the idinfo of their binders.
-- (Even more, set occurences of variables to point to their binders!)
-- This may mean the result is not alpha-equivalent to the input.
-- However, I don't know if GHC ever cares about this.
-- I believe that in early passes, occurences have no idinfo, but I don't know if there is ever
-- a case where the occurence has different info to the binder (rather than just has no info at all).
-- The reason we do this is because var occurences inside unfoldings can have unfoldings attached,
-- and if we did not overwrite them, we would have to recursively analyse them for shadowing information
-- (unfoldings need not be closed terms). Notably, we could get away with only setting idinfo of occurences
-- inside unfoldings, rather than everywhere. We choose not to for simplicity.

-- TODO: the idea that we only need to care about the allowedShadowInfo of an IdInfo on a /binder/
-- may be flawed: consider a non-locally bound binder which has a recursive unfolding, and we happen
-- to capture one of its free variables, say
{-
    foo = baz $ \x -> bar [Unf = bar baz] x
  Then we potentially could rename x |-> baz, as we don't analyse the unfolding...
-}
-- We could avoid this by only renaming to shadow local variables, but that seems to be
-- rather unambitious!

{-# LANGUAGE GeneralisedNewtypeDeriving, TupleSections #-}
module Shadowing (plugin) where

import GhcPlugins hiding ((<>))
import TyCoRep
import UniqDFM
import UniqDSet
import Unique (getKey)

import Data.Bifunctor (second)
import Data.List (mapAccumL)
import Data.Maybe (fromJust,mapMaybe,isJust)
import qualified Data.Set as S

plugin :: Plugin
plugin = defaultPlugin {
  installCoreToDos = install
  ,pluginRecompile = purePlugin
  }

install :: [CommandLineOption] -> [CoreToDo] -> CoreM [CoreToDo]
{-
Let's run the plugin as often as possible, as GHC often unshadows binders (e.g. when doing substitution).
NB: we don't run as the first pass, to avoid spurious lint warnings about "INLINE binder is (non-rule) loop breaker".
These arise because the desugarer generates these for instance methods, but the check is turned off for
the output of the desugarer. The first pass is usually a run of the simplifier, which eliminates them.
-- cf ghc's Note [Checking for INLINE loop breakers]

On a very simple test: foo x y = y, it appears that the following passes deshadow binds:
  Specialise
  Levels added and Float Out -- These seem to be one pass
  Common sub-expression
  SpecConstr
  CorePrep
And these don't deshadow our example (maybe because they just don't touch foo?):
  Float inwards
  Called arity analysis
  Demand analysis
  Worker Wrapper binds
  Exitification transformation
  Liberate case
  Tidy Core

NB: the pipeline is Desugar -> DesugarOpt -> passes & plugins -> CoreTidy -> CorePrep
Thus, since CorePrep seems to un-shadow, the stg/cmm layers will never see it!
-}

install _opts todo = do
  let new_todo = concatMap (\(n,p) -> [p, CoreDoPluginPass ("Shadowing (" ++ show n ++ ") - post " ++ showSDocUnsafe (ppr p)) pass]) $ zip [0::Int ..] todo
  pure $ new_todo

pass :: ModGuts -> CoreM ModGuts
pass = bindsOnlyPass $ \bs -> pure $ renameProgram bs $ computeRenaming $ analyseProgram bs


{-
For each variable we calculate the variables it is able to shadow,
and also the variables it must not shadow.
For instance, \a b c. b + c gives
a |-> ([],[])
b |-> ([a],[])
c |-> ([a],[b])
saying that we could change the unique on b (or c) to shadow a,
but we must not do this on both, otherwise c would shadow b!
These two variable sets should partition the context of the binder.
-}

{- ANALYSIS
Strategy:
Go over the whole program creating a mapping from binding uniques to contexts they appear in, and things they scope over.
We look into the IdInfo of a binder, but not the IdInfo of an occurence.
However, when seeing an occurence, we record that the things free in the IdInfo of the relevent binder should be considered free
in the current scope.
(NB: we care about the info attached to the binder, not the occurence, since when renaming, we will overwrite the info on the occurence
with that of the binder.)
We form a mapping
unique |-> [(cxt,idinfo-first-order-fvs,scope-fvs)], where cxt is a set of uniques, *-fvs is a set of either uniques or INFO(unique).
Note that this is a list! We may have, on previous shadowing passes, given two completely unrelated binders the same unique.
(Perhaps they both shadow the same library definition.
 It appears that, in pathological situations, we may be able to get "more" shadowing if we renamed the two binder-occurences of
 the same unique differently, as a first pass.
 TODO: is there any point in doing this?
)
Going over this map, if we have x |-> (Ga,FVI,FV), then (FVI,FV)\(Ga,x) are defined out-of-module, let's collect these, as Glbls.
From now on, we will not bother to tell different binders with the same unique apart, so let's collapse the list.
Now, we know to first-order the vars that may appear in IdInfos, so close under that (iterate until fixed point).
Now we can throw away the idinfo-fvs, as they have done their job.
We get left with a map
unique |-> (cxt,fvs)
Now for each x|->(Ga,FV), we know it would be nice if x shadowed something in (Ga,Glbls)\FV, but must not shadow FV\x

From this shadowing information, it is easy to find some mapping old-unique |-> new-unique that increases shadowing.
TODO: is there a better strategy that the obvious greedy one?

Now go back over the program, renaming every binder and occurence, according to our new unique mapping.
We must recurse into unfoldings, otherwise they are inconsistent, however, this could lead to an infinite loop.
Thus, take a leaf from CoreSubsts' book, and every tie the knot: variable occurences will be set to point at their binder.
Note that we only rename locally-bound vars, so need not recurse into IdInfo of out-of-module Vars.
Thus if we have not find an occurence of a variable we have not gone under a binder for, we can leave it alone.
-}

type UniqueSet = UniqSet Unique

{- Record free variables:
U: this variable occurs free
IDINFO v: all the variables that occur free in the IdInfo of v's binder should be considered free here
-}
data FVInfo = U Unique | IDINFO Unique deriving Eq
instance Outputable FVInfo where
  ppr (U x) = text "U" <+> ppr x
  ppr (IDINFO x) = text "IDINFO" <+> ppr x

instance Ord FVInfo where
  compare (U a) (U b) = compare (getKey a) (getKey b)
  compare (U _) (IDINFO _) = LT
  compare (IDINFO _) (U _) = GT
  compare (IDINFO a) (IDINFO b) = compare (getKey a) (getKey b)
data BndrInfo1 = BndrInfo1 (UniqFM [(UniqueSet, S.Set FVInfo, S.Set FVInfo)]) -- cxt, first order IdInfo fvs, first order scope fvs
instance Outputable BndrInfo1 where
  ppr (BndrInfo1 x) = ppr x
instance Semigroup BndrInfo1 where
  BndrInfo1 a <> BndrInfo1 b = BndrInfo1 $ plusUFM_C (++) a b
instance Monoid BndrInfo1 where
  mempty = noBndrInfo
-- we collect ShadowInfo1 this when analysing: first-order info about the binders, free vars & references to IdInfos, the global vars
newtype ShadowInfo1 = ShInfo1 (BndrInfo1, S.Set FVInfo, VarSet)
  deriving (Semigroup, Monoid, Outputable)

data ShadowInfo = ShInfo (UniqFM (UniqueSet, UniqueSet)) -- "good" and "bad"
instance Outputable ShadowInfo where
  ppr (ShInfo x) = ppr x

fvInfoIsU :: FVInfo -> Bool
fvInfoIsU (U _) = True
fvInfoIsU (IDINFO _) = False

filterUFVInfo :: S.Set FVInfo -> UniqueSet
filterUFVInfo = mkUniqSet . mapMaybe f . S.toList
  where f (U u) = Just u
        f (IDINFO _) = Nothing

updateAllFVInfo :: UniqFM UniqueSet -- how to replace each IDINFO
                -> S.Set FVInfo
                -> UniqueSet
updateAllFVInfo r = unionManyUniqSets . map f . S.toList
  where f (U u) = unitUniqSet u
        f (IDINFO u) | Just u' <- lookupUFM r u = u'
                     | otherwise = error "updateAllFVInfo: replacement map not complete"

{-
We have a description of sets X_i = A_i + rec_i,
where A_i is statically known, and rec_i is a union of some X_j s
We compute what each X_i is, by continuously unfolding the recursion until we
hit a fixed point. NB, if rec_i contains "X_i", then we can ignore that term!

The term "fixed point" is a bit of a lie here, we are actually doing a reachability analysis on a graph,
so want to iterate substituting RHS for LHS n times. This may well leave references rec_i (i.e. IDINFO terms),
but we know that after unfolding n times, they will never actually add anything.

We have 2 ufms: one of closed vars (no recursive references), one under construction (with recursive references)
Do "repeated squaring" log n times.
-}

closeFVInfo :: UniqFM (S.Set FVInfo) -> UniqFM UniqueSet
closeFVInfo xs = let (closed,open) = partitionUFM (all fvInfoIsU) xs
                 in go closed open 1
  where stop = 2 * sizeUFM xs
        -- Invariant: closed should only consist of U _, and no IDINFO _
        -- (hence 'closed': i.e. don't have any references to the IdInfo of other variables to consider)
        go closed open n | n >= stop = plusUFM (mapUFM filterUFVInfo open) (mapUFM filterUFVInfo closed)
                         | otherwise = let update (U u) = S.singleton $ U u
                                           update (IDINFO v) = case (lookupUFM closed v, lookupUFM open v) of
                                             (Just x,_) -> x
                                             (Nothing, Just y) -> y
                                             (Nothing, Nothing) -> error "closeFVInfo: map is not complete!"
                                           new = mapUFM_Directly (\u s -> S.delete (IDINFO u) s) -- remove self-references
                                               $ mapUFM (foldl S.union S.empty . S.map update) open
                                           (closed',open') = partitionUFM (all fvInfoIsU) new
                                           new_closed = plusUFM closed closed'
                                       in go new_closed open' (2*n)

analyseProgram :: CoreProgram -> ShadowInfo
analyseProgram p = let ShInfo1 (BndrInfo1 cxt_info_scope,_,glblVars) = analyseTop p
                       cxt_info_scope_collapsed = mapUFM mconcat cxt_info_scope -- monoid is union
                       idinfo = mapUFM (\(_,i,_) -> i) cxt_info_scope_collapsed
                       -- TODO: idFreeVars does not report any free vars in the unfolding of a out-of-module defined thing,
                       -- since they are GlobalIds, and thus considered constants.
                       -- I expect I need to ensure I don't shadow these, but perhaps they are in a different name-space and I don't have to
                       -- Let's try that first - just ignore globals when closeing our idinfo!
                       forgetGlobals = S.filter $ \v -> case v of
                                                          U _ -> True
                                                          IDINFO u -> not $ u `elemVarSetByKey` glblVars
                       idinfoFull :: UniqFM UniqueSet -- for each binder, all vars free in its IdInfo
                       idinfoFull = closeFVInfo $ mapUFM forgetGlobals idinfo
                       cxtscope = mapUFM (second forgetGlobals) $ mapUFM (\(c,_,s) -> (c,s)) cxt_info_scope_collapsed
                       cxtscopeFull :: UniqFM (UniqueSet,UniqueSet)
                       cxtscopeFull = mapUFM (second $ updateAllFVInfo idinfoFull) cxtscope
                   in ShInfo cxtscopeFull

analyseTop :: CoreProgram -> ShadowInfo1
analyseTop p = let a = go emptyUniqSet p
               -- core lint complains that "Duplicate variables brought into scope [[quux, $trModule]]"
               -- if top level binds shadow each other
                   b = noMutualShadow (bindersOfBinds p)
               in a <> b
  where go _ [] = ShInfo1 (noBndrInfo,S.empty,mempty)
        go cxt (NonRec b e : rest) = analyseBinders cxt NonRecBind [b] $ \cxt' -> analyseExpr cxt' e <> go cxt' rest
        go cxt (Rec bes : rest) = analyseBinders cxt RecBind (map fst bes) $ \cxt' -> foldMap (analyseExpr cxt' . snd) bes <> go cxt' rest


-- In general, we take in a context and a term to analyse
-- and return the BndrInfo1 for all binders in that term, and the free vars of that term

noMutualShadow :: [CoreBndr] -> ShadowInfo1
-- Say that each binder b appears in the empty context, and scopes over a use of each other one
noMutualShadow bs = ShInfo1 (BndrInfo1 $ listToUFM $ map (\(b,others) -> (b,[(mempty,mempty,S.fromList $ map (U . getUnique) others)])) $ selectEach bs
                            ,mempty
                            ,mempty)

selectEach :: [a] -> [(a,[a])]
selectEach [] = []
selectEach (x:xs) = (x,xs) : map (second (x:)) (selectEach xs)

noBndrInfo :: BndrInfo1
noBndrInfo = BndrInfo1 emptyUFM

uniqueUse :: Unique -> ShadowInfo1
uniqueUse u = ShInfo1 (noBndrInfo, S.fromList [U u], mempty)

varUse :: UniqueSet -> Var -> ShadowInfo1
-- TODO: would it be worth recording we have fvs that include the fvs of the type of the binder for u,
-- similar to the fvs of the IdInfo (for efficiency only, rather than to avoid a loop)?
varUse cxt v = let (ShInfo1 (tybndrs,tyfvs,tyglbl)) = analyseTy cxt (idType v)
               in ShInfo1 (tybndrs
                          ,S.insert (U u) $ S.insert (IDINFO u) tyfvs
                          ,if isGlobalId v then addOneToUniqSet tyglbl v else tyglbl
                          )
  where u = varUnique v

noVars :: ShadowInfo1
noVars = ShInfo1 (noBndrInfo, S.empty, mempty)

data RecBind = RecBind | NonRecBind
analyseBinders :: UniqueSet
               -> RecBind -- is this a recursive group, i.e. do they scope over their own IdInfo
               -> [Var] -- the vars
               -> (UniqueSet -> ShadowInfo1) -- what to do in the extended context
               -> ShadowInfo1
analyseBinders cxt r vs scope = let cxtExt = addListToUniqSet cxt $ map getUnique vs
                                    cxt' = case r of
                                             NonRecBind -> cxt
                                             RecBind -> cxtExt
                                    typ = foldMap (analyseTy cxt . idType) vs
                                    info@(ShInfo1 (_, fvinfo,_)) = foldMap (analyseIdInfo cxt') vs
                                    rmVs2 fvs = foldr S.delete fvs $ map (U . getUnique) vs
                                    rmVs (ShInfo1 (bi,fvs,glbls)) = ShInfo1 (bi,rmVs2 fvs,glbls)
                                    info' = case r of
                                              NonRecBind -> info
                                              RecBind -> rmVs info
                                    scope'@(ShInfo1 (_,fvscope,_)) = scope cxtExt
                                    scope'' = rmVs scope'
                                    bndrs = BndrInfo1 $ listToUFM $ map (\v -> (v,[(cxt,fvinfo,S.union fvinfo fvscope)])) vs
                                in typ <> info' <> scope'' <> ShInfo1 (bndrs, mempty, mempty)

analyseExpr :: UniqueSet -> CoreExpr -> ShadowInfo1
analyseExpr cxt (Var v) = varUse cxt v
analyseExpr _cxt (Lit _) = noVars
analyseExpr cxt (App f e) = analyseExpr cxt f <> analyseExpr cxt e
analyseExpr cxt (Lam v e) = analyseBinders cxt NonRecBind [v] $ \cxt' -> analyseExpr cxt' e
analyseExpr cxt (Let (NonRec b e) t) = analyseExpr cxt e <> analyseBinders cxt NonRecBind [b] (\cxt' -> analyseExpr cxt' t)
analyseExpr cxt (Let (Rec bes) t) = let (bs,es) = unzip bes
                                    in analyseBinders cxt RecBind bs $ \cxt' -> foldMap (analyseExpr cxt') es <> analyseExpr cxt' t
analyseExpr cxt (Case e x _T alts) = let ae = analyseExpr cxt e
                                         aT = analyseTy cxt _T
                                         ax = analyseBinders cxt NonRecBind [x] $ \cxt' -> foldMap (altAS cxt') alts
                                     in ae <> aT <> ax
  -- TODO: don't know how to classify bs as Rec or not
  where altAS cxt' (_,bs,t) = analyseBinders cxt' NonRecBind bs $ \cxt'' -> analyseExpr cxt'' t
analyseExpr cxt (Cast e co) = analyseExpr cxt e <> analyseCo cxt co
analyseExpr cxt (Tick tk e) = analyseTickish cxt tk <> analyseExpr cxt e
analyseExpr cxt (Type _T) = analyseTy cxt _T
analyseExpr cxt (Coercion co) = analyseCo cxt co

analyseTy :: UniqueSet -> Type -> ShadowInfo1
analyseTy cxt (TyVarTy v) = varUse cxt v
analyseTy cxt (AppTy _S _T) = analyseTy cxt _S <> analyseTy cxt _T
analyseTy cxt (TyConApp _tc _Ts) = mconcat $ map (analyseTy cxt) _Ts
analyseTy cxt (ForAllTy alpha' _T) = let alpha = binderVar alpha'
                                     in analyseBinders cxt NonRecBind [alpha] $ \cxt' -> analyseTy cxt' _T
analyseTy cxt (FunTy _S _T) = analyseTy cxt _S <> analyseTy cxt _T
analyseTy _cxt (LitTy _) = noVars
analyseTy cxt (CastTy _T co) = analyseTy cxt _T <> analyseCo cxt co
analyseTy cxt (CoercionTy co) = analyseCo cxt co

analyseCo :: UniqueSet -> Coercion -> ShadowInfo1
analyseCo cxt (Refl _ _T) = analyseTy cxt _T
analyseCo cxt (TyConAppCo _ _ coz) = foldMap (analyseCo cxt) coz
analyseCo cxt (AppCo co1 co2) = analyseCo cxt co1 <> analyseCo cxt co2
analyseCo cxt (ForAllCo alpha coK co) = analyseBinders cxt NonRecBind [alpha] $ \cxt' ->
                                               analyseCo cxt' coK
                                            <> analyseCo cxt' co
analyseCo cxt (FunCo _ coS coT) = analyseCo cxt coS <> analyseCo cxt coT
analyseCo cxt (CoVarCo v) = varUse cxt v
analyseCo cxt (AxiomInstCo _ _ coz) = foldMap (analyseCo cxt) coz
analyseCo cxt (AxiomRuleCo _ coz) = foldMap (analyseCo cxt) coz
analyseCo cxt (UnivCo prov _ _S _T) = analyseProv prov
                                   <> analyseTy cxt _S
                                   <> analyseTy cxt _T
  where analyseProv UnsafeCoerceProv = noVars
        analyseProv (PhantomProv co) = analyseCo cxt co
        analyseProv (ProofIrrelProv co) = analyseCo cxt co
        analyseProv (PluginProv _) = noVars
analyseCo cxt (SymCo co) = analyseCo cxt co
analyseCo cxt (TransCo co1 co2) = analyseCo cxt co1 <> analyseCo cxt co2
analyseCo cxt (NthCo _ _ co) = analyseCo cxt co
analyseCo cxt (LRCo _ co) = analyseCo cxt co
analyseCo cxt (InstCo faCo inCo) = analyseCo cxt faCo <> analyseCo cxt inCo
analyseCo cxt (CoherenceCo co1 co2) = analyseCo cxt co1 <> analyseCo cxt co2
analyseCo cxt (KindCo co) = analyseCo cxt co
analyseCo cxt (SubCo co) = analyseCo cxt co
analyseCo _ (HoleCo _) = error "By the time we hit core, we should not have HoleCo"

analyseTickish :: UniqueSet -> Tickish Id -> ShadowInfo1
analyseTickish _ (ProfNote _ _ _) = noVars
analyseTickish _ (HpcTick _ _) = noVars
analyseTickish cxt (Breakpoint _ fvs) = foldMap (varUse cxt) fvs
analyseTickish _ (SourceNote _ _) = noVars


analyseIdInfo :: UniqueSet -> Var -> ShadowInfo1
analyseIdInfo cxt i | isId i = unfShadows <> ruleShadows
  where info = idInfo i
        rule = ruleInfo info
        unf = unfoldingInfo info
        unf_expr = maybeUnfoldingTemplate unf
        unfShadows = maybe mempty (analyseExpr cxt) unf_expr
        ruleShadows = foldMap ruleSh $ ruleInfoRules rule
        ruleSh (BuiltinRule{}) = mempty
        ruleSh (Rule _ _ fn _ bndrs args rhs _ _ _ _)
          = uniqueUse (getUnique fn) <> analyseBinders cxt NonRecBind bndrs (\cxt' -> foldMap (analyseExpr cxt') (rhs:args))
analyseIdInfo _ _ | otherwise = mempty


{- COMPUTE THE RENAMING -}

computeRenaming :: ShadowInfo -> UniqFM Unique
computeRenaming (ShInfo xs) = getRename xs

-- For each variable, we are given some variables we would like to have the same unique as, and some which we must not.
-- We return a mapping which updates each variable's unique.
-- TODO: it is not clear how to do this to obtain "maximal" shadowing. Currently we do a simple-minded greedy algorithm.
getRename :: UniqFM (UniqueSet,UniqueSet) -> UniqFM Unique
-- We maintain sets of variables we would like to give the same unique.
-- We iteratively merge sets to increase shadowing, ensuring that we don't merge
-- sets containing variables that must not shadow each other.
getRename sh = let (good',bad') = unzip $ map (\(v,(gs,bs)) -> (map (\g -> (v, g)) (nonDetKeysUniqSet gs)
                                                               ,map (\b -> (v, b)) (nonDetKeysUniqSet bs)))
                                        $ nonDetUFMToList sh
                   -- We could have (from a previous pass) \x . \x . x, and thus think it would be good
                   -- to rename (the inner) x to shadow (the outer) x. This is silly, and breaks an assumption in go!
                   good = filter (uncurry (/=)) $ concat good'
                   emptyBad = mapUFM (const emptyUniqSet) sh -- listToUFM $ map (\v -> (v,emptyUniqSet)) $ M.keys sh
                   bad = plusUFM_C unionUniqSets emptyBad $ listToUFM_C unionUniqSets $ concatMap (\(u,v) -> [(u,unitUniqSet v),(v,unitUniqSet u)]) $ concat bad'
                   sets = addListToUDFM emptyUDFM $ map (\v -> (v, (unitUniqSet v))) $ nonDetKeysUFM sh
               in toMap $ go True good bad sets
  -- The boolean is to do two passes:
  -- firstly if merging two sets introduces shadowing, do it
  -- secondly if merging two sets doesn't introduce shadowing but is not inconsistent, do it
  where isLocal :: Unique -> Bool
        isLocal = isJust . lookupUFM sh
        go :: Bool -> [(Unique,Unique)] -> UniqFM UniqueSet -> UniqDFM UniqueSet -> UniqDFM UniqueSet
        -- invariant: bad is symmetric: if y`elem`lookup bad x, then x`elem`lookup bad y NOT quite TRUE: lookup bad y may fail!
        -- invariant: the left elt of something in good is defined-in-this-module variable (but the right may be external)
        -- invariant: all local vars have an entry in bad
        go False [] _ uniqs = uniqs
        go True [] bad uniqs = let us = map fst $ udfmToList uniqs
                                   safe = [(x,y) | x <- us, y <- us, x /= y, not (y`elemUniqSet_Directly`lookupUFMUnsafe bad x)]
                               in go False safe bad uniqs
        -- If there is shadowing already in the program, and we have a pattern like \x \y \x . y,
        -- we may think that y can shadow x (from \x \y ...) , but x must not shadow y (from \y \x ...).
        go p ((x,y):good) bad uniqs
          | Just badY <- lookupUFM bad y, elemUniqSet_Directly x badY = go p good bad uniqs -- we shouldn't shadow y
          -- If lookupUFM bad y == Nothing, then y is defined at top-level, and we can shadow it

        -- Check the invariant that if we believe x may shadow y, we don't also believe that x must not shadow y.
        go _ ((x,y):_) bad _ | elemUniqSet_Directly y (lookupUFMUnsafe bad x)
                             = error "getRename: inconsistent beliefs!"
        go p ((x,y):good) bad uniqs = let badX = lookupUFMUnsafe bad x
                                          bad' = mapUFM (\b -> if elemUniqSet_Directly x b then addOneToUniqSet (delOneFromUniqSet_Directly b x) y else b)
                                               $ adjustUFM (unionUniqSets badX) (delFromUFM bad x) y
                                          xToY u = if u == x then y else u
                                          ok (u,v) = isLocal u && not (elemUniqSet_Directly v $ lookupUFMUnsafe bad' u)
                                          good' = filter ok $ mapMaybe (\(u,v) -> let u' = xToY u
                                                                                      v' = xToY v
                                                                                  in if u' == v' then Nothing else Just(u',v')) good
                                          uniqX = lookupUDFMUnsafe uniqs x
                                          uniqs' = adjustUDFM (unionUniqSets uniqX) (delFromUDFM uniqs x) y
                                      in go p good' bad' uniqs'
        toMap :: UniqDFM UniqueSet -> UniqFM Unique
        toMap rn = listToUFM_Directly $ concatMap (\(u,vs) -> map (\v -> (v,u)) $ nonDetEltsUniqSet vs) $ udfmToList rn

lookupUFMUnsafe :: UniqFM a -> Unique -> a
lookupUFMUnsafe m k = fromJust (lookupUFM m k)

lookupUDFMUnsafe :: UniqDFM a -> Unique -> a
lookupUDFMUnsafe m k = fromJust (lookupUDFM m k)

-- Whilst our renaming should contain all variables bound in this module,
-- it won't contain imported functions.
(!?) :: UniqFM Unique -> Unique -> Unique
m !? k = lookupWithDefaultUFM_Directly m k k

{- DO THE RENAMING -}
renameProgram :: CoreProgram -> UniqFM Unique -> CoreProgram
-- TODO: common up with renameBinds?
renameProgram p rn = snd $ mapAccumL (\vars b -> go b vars) emptyUFM p
  where go (NonRec b e) vars = let (vars', b') = renameBndr rn vars b
                               in (vars', NonRec b' (rename rn vars' e))
        go (Rec bes) vars  = let (bs,es) = unzip bes
                                 (vars',bs') = renameRecBndrs rn vars bs
                                 es' = map (rename rn vars') es
                                 bes' = zip bs' es'
                             in (vars', Rec bes')

-- We also rename inside the varType, to keep the cached information up-to-date
-- and the IdInfo

renameBndr :: UniqFM Unique -> UniqFM Var -> Var -> (UniqFM Var, Var)
renameBndr rn vars v = let (vars',[v']) = renameRecBndrs rn vars [v]
                       in (vars',v')

renameRecBndrs :: UniqFM Unique -> UniqFM Var -> [Var] -> (UniqFM Var, [Var])
renameRecBndrs rn vars vs = let (vars',vs') = mapAccumL (go vars') vars vs
                            in (vars',vs')
  -- we are knot-tying here
  where go :: UniqFM Var -> UniqFM Var -> Var -> (UniqFM Var,Var)
        go rec_vars vars' v = let v' = updateIdInfo (renameIdInfo rn rec_vars) $ updateVarType (renameTy rn rec_vars) $ setVarUnique v (rn !? getUnique v)
                                  vars'' = addToUFM vars' v v'
                              in (vars'', v')
        updateIdInfo :: (IdInfo -> IdInfo) -> Var -> Var
        updateIdInfo f w | isId w = lazySetIdInfo w $ f (idInfo w)
                         | otherwise = w

renameVar :: UniqFM Var -> Var -> Var
renameVar vars v | Just v' <- lookupUFM vars v = v'
                 -- Otherwise, it is not in the scope of our renaming, and thus in its type/idinfo cannot have
                 -- captured any of the uniques which we change. Thus nothing to do.
                 | otherwise = v

rename :: HasCallStack => UniqFM Unique -> UniqFM Var -> CoreExpr -> CoreExpr
rename _rn vars (Var v) = Var $ renameVar vars v
rename _rn _vars l@(Lit _) = l
rename rn vars (App f e) = App (rename rn vars f) (rename rn vars e)
rename rn vars (Lam v e) = let (vars',v') = renameBndr rn vars v
                           in Lam v' (rename rn vars' e)
-- TODO : common up with renameProgram?
rename rn vars (Let (NonRec b e) t) = let (vars',b') = renameBndr rn vars b
                                      in Let (NonRec b' (rename rn vars' e)) (rename rn vars' t)
rename rn vars (Let (Rec bes) t) = let (bs,es) = unzip bes
                                       (vars',bs') = renameRecBndrs rn vars bs
                                       es' = map (rename rn vars') es
                                   in Let (Rec $ zip bs' es') (rename rn vars' t)
rename rn vars (Case e x _T alts) = let (vars',x') = renameBndr rn vars x
                                    in Case (rename rn vars e) -- NB: x does not scope over this, so use old vars
                                         x'
                                         (renameTy rn vars' _T)
                                         (map (renameAlts vars') alts)
  where renameAlts vars' (con,binds,t) = let (vars'',binds') = mapAccumL (renameBndr rn) vars' binds
                                         in (con, binds', rename rn vars'' t)

rename rn vars (Cast e co) = Cast (rename rn vars e) (renameCo rn vars co)
rename rn vars (Tick tk e) = Tick (renameTickish vars tk) (rename rn vars e)
rename rn vars (Type _T) = Type (renameTy rn vars _T)
rename rn vars (Coercion co) = Coercion (renameCo rn vars co)

renameTy :: UniqFM Unique -> UniqFM Var -> Type -> Type
renameTy _rn vars (TyVarTy v) = TyVarTy $ renameVar vars v
renameTy rn vars (AppTy _S _T) = AppTy (renameTy rn vars _S) (renameTy rn vars _T)
renameTy rn vars (TyConApp tc _Ts) = TyConApp tc (map (renameTy rn vars) _Ts)
renameTy rn vars (ForAllTy alpha _T) = let (vars',alpha') = renameBndr rn vars $ binderVar alpha
                                       in ForAllTy (mkTyVarBinder (binderArgFlag alpha) alpha')
                                                   (renameTy rn vars' _T)
renameTy rn vars (FunTy _S _T) = FunTy (renameTy rn vars _S) (renameTy rn vars _T)
renameTy _rn _vars l@(LitTy _) = l
renameTy rn vars (CastTy _T co) = CastTy (renameTy rn vars _T) (renameCo rn vars co)
renameTy rn vars (CoercionTy co) = CoercionTy (renameCo rn vars co)

renameCo :: UniqFM Unique -> UniqFM Var -> Coercion -> Coercion
renameCo rn vars (Refl r _T) = Refl r (renameTy rn vars _T)
renameCo rn vars (TyConAppCo r tc coz) = TyConAppCo r tc $ map (renameCo rn vars) coz
renameCo rn vars (AppCo co1 co2) = AppCo (renameCo rn vars co1) (renameCo rn vars co2)
renameCo rn vars (ForAllCo alpha coK co) = let (vars',alpha') = renameBndr rn vars alpha
                                           in ForAllCo alpha' (renameCo rn vars' coK) (renameCo rn vars' co)
renameCo rn vars (FunCo r coS coT) = FunCo r (renameCo rn vars coS) (renameCo rn vars coT)
renameCo _rn vars (CoVarCo v) = CoVarCo (renameVar vars v)
renameCo rn vars (AxiomInstCo rule n coz) = AxiomInstCo rule n (map (renameCo rn vars) coz)
renameCo rn vars (AxiomRuleCo rule coz) = AxiomRuleCo rule (map (renameCo rn vars) coz)
renameCo rn vars (UnivCo prov r _S _T) = UnivCo (renameProv prov) r (renameTy rn vars _S) (renameTy rn vars _T)
  where renameProv UnsafeCoerceProv = UnsafeCoerceProv
        renameProv (PhantomProv co) = PhantomProv (renameCo rn vars co)
        renameProv (ProofIrrelProv co) = ProofIrrelProv (renameCo rn vars co)
        renameProv (PluginProv s) = PluginProv s
renameCo rn vars (SymCo co) = SymCo (renameCo rn vars co)
renameCo rn vars (TransCo co1 co2) = TransCo (renameCo rn vars co1) (renameCo rn vars co2)
renameCo rn vars (NthCo r n co) = NthCo r n (renameCo rn vars co)
renameCo rn vars (LRCo lr co) = LRCo lr (renameCo rn vars co)
renameCo rn vars (InstCo faCo inCo) = InstCo (renameCo rn vars faCo) (renameCo rn vars inCo)
renameCo rn vars (CoherenceCo co1 co2) = CoherenceCo (renameCo rn vars co1) (renameCo rn vars co2)
renameCo rn vars (KindCo co) = KindCo (renameCo rn vars co)
renameCo rn vars (SubCo co) = SubCo (renameCo rn vars co)
renameCo _ _ (HoleCo _) = error "By the time we hit core, we should not have HoleCo"

renameTickish :: UniqFM Var -> Tickish Id -> Tickish Id
renameTickish _vars tk@(ProfNote _ _ _) = tk
renameTickish _vars tk@(HpcTick _ _) = tk
renameTickish vars (Breakpoint bpId fvs) = Breakpoint bpId $ map (renameVar vars) fvs
renameTickish _vars tk@(SourceNote _ _) = tk

renameIdInfo :: UniqFM Unique -> UniqFM Var -> IdInfo -> IdInfo
renameIdInfo rn vars info = info `setUnfoldingInfo` unfolding' `setRuleInfo` rule'
  where rule' = case ruleInfo info of
          RuleInfo rules fvs -> RuleInfo (map renameRule rules) (mapUniqDSet (renameVar vars) fvs)
        renameRule r@(BuiltinRule{}) = r
        renameRule (Rule name act fn rough bndrs args rhs auto origin orphan local)
          = Rule name
                 act
                 (renameName rn fn)
                 (map (fmap (renameName rn)) rough)
                 (map (renameVar vars) bndrs)
                 (map (rename rn vars) args)
                 (rename rn vars rhs)
                 auto
                 origin
                 orphan
                 local
        unfolding' = case unfoldingInfo info of
          NoUnfolding -> NoUnfolding
          BootUnfolding -> BootUnfolding
          OtherCon as -> OtherCon as
          DFunUnfolding bndrs con args -> DFunUnfolding (map (renameVar vars) bndrs) con (map (rename rn vars) args)
          CoreUnfolding tmpl src is_top is_value is_conlike is_work_free expandable guidance ->
            CoreUnfolding (rename rn vars tmpl) src is_top is_value is_conlike is_work_free expandable guidance

renameName :: UniqFM Unique -> Name -> Name
renameName rn n = setNameUnique n (rn !? getUnique n)

-- This will be defined by GHC in 8.8.1
mapUniqDSet :: Uniquable b => (a -> b) -> UniqDSet a -> UniqDSet b
mapUniqDSet f = mkUniqDSet . map f . uniqDSetToList
